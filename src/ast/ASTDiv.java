package ast;

import compiler.CodeBlock;
import util.DuplicateIdentifierException;
import util.Environment;
import util.ExecutionErrorException;
import util.UndeclaredIdentifierException;

public class ASTDiv implements ASTNode {

	ASTNode left, right;

	public ASTDiv(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	@Override
	public int eval(Environment<Integer> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, ExecutionErrorException {
		return left.eval(env) / right.eval(env);
	}

	@Override
	public void compile(CodeBlock code) {
		this.left.compile(code);
		this.right.compile(code);
		code.emit_div();
	}

	@Override
	public String toString() {
		return left.toString() + " / " + right.toString();
	}
}
