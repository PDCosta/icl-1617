package ast;

import compiler.CodeBlock;
import util.Environment;

public class ASTNum implements ASTNode {

	int val;

	public int eval(Environment<Integer> env) {
		return val;
	}

	public ASTNum(int n) {
		val = n;
	}

	@Override
	public void compile(CodeBlock code) {
		code.emit_push(val);
	}

	@Override
	public String toString() {
		return Integer.toString(val);
	}
}
